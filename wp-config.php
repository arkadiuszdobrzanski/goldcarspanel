<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */
ini_set('display_errors','Off');
ini_set('error_reporting', E_ALL );
define('WP_DEBUG', true);
define('WP_DEBUG_DISPLAY', true);
// ** MySQL settings ** //
/** The name of the database for WordPress */
define( 'DB_NAME', "local" );
//define( 'DB_NAME', "admin_goldpanel" );
define( 'DB_USER', "root" );
//define( 'DB_USER', "admin_goldpanel" );
define( 'DB_PASSWORD', "root" );
//define( 'DB_PASSWORD', "NoweHaslo123@" );

/** MySQL hostname */
define( 'DB_HOST', "localhost" );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',          '8cN<fw)DJIO#M,w3!12 @WP+[&wV9lnF?5jN55ZPt*-%3 I>u_Wh0`{V%E;=<.YV' );
define( 'SECURE_AUTH_KEY',   'SUm#-_Q:Hf@_(h0^.)DgDX>s?VORN+A9SkidN2^Nj,$&p<VlkaUi|!ucN=UgLww`' );
define( 'LOGGED_IN_KEY',     '$7_-=Q9ZtUQH_^BFylA#k^IR8>W5.r9Nrgc0(PZ2Cga eykL0DP>{lFf}/y(D@A$' );
define( 'NONCE_KEY',         'cIl6eBKI%y42B^zn{3J>9NqESe,dYSQI1P#nlX<s{L@+{Y%XaFzB%e+@0BZiLTlu' );
define( 'AUTH_SALT',         '[v(t/Ett=tcKu95bxbUI^q?ILmBZ#mh2j0Fy;0A*c?([l5WbGHv}eB6m=V@ZnJSZ' );
define( 'SECURE_AUTH_SALT',  ' `^aD+QJ,/b`B:4)1LE81aIO&9l7bHY1l%,.=*ThjoLO<?vrFMXxx_8:>|,AK(wj' );
define( 'LOGGED_IN_SALT',    'TEc;>?>RoH qV=Yj;}QBrq^ep[G!oy:^:0C*+X%^5mD6IHJ^E8Q-gU)8cPiFiRr&' );
define( 'NONCE_SALT',        ':g VxV2pay{[=.=i,(P b|&P6C_`)35J4tK&F5Sdc1JPRg,EQjlDRx?aEU,*42,v' );
define( 'WP_CACHE_KEY_SALT', 'BkOG8@l:l(7ulby|%~lgyf.~c{dC:I%i=E> i_@b $O7|EJT-alO3<]CmB?%7M_<' );

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) )
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
