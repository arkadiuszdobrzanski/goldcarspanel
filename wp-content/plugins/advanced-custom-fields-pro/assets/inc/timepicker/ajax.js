

jQuery(function($) {
    function submitData(method, searchValue, url, fill) {
        request = $.ajax({
            //uderzam do customowego routingu
            url: url + "/" + searchValue,
            type: method,
            data: searchValue,
            fail: function () {},
            success: function (data) {
                //w data jest zwracany cały wygląd HTML wraz z wprowadzonymi danymi
                console.log(searchValue);
                $(fill).hide().fadeOut(250).html(data).fadeIn(350);
                //Więc wrzucam 'data' do tej klasy
            },
        });
    }
});