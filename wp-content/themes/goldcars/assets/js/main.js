(function ($) {

  $(document).ready(function () {
    $('.drawer-toggle').click(function () {
      $('body').toggleClass('drawer-open');
    });

    $('.page_item_has_children a').click(function (e) {
      e.preventDefault();
      const child = $(this).parent().find('.children').first();
      child.slideToggle();
      $(this).parent().toggleClass('open');
    });


    $("#sticky").stick_in_parent({
      offset_top: 20,
    });
    // Modal
    $('.single-car .blackInfo .btn-reservation .modal-body .modal-right a').click(function (event) {
      event.preventDefault();
      $('.single-car .blackInfo .btn-reservation .modal-body .modal-left').slideToggle();
      $('.single-car .blackInfo .btn-reservation .modal-body .modal-center').slideToggle();
      $('.single-car .blackInfo .btn-reservation .modal-body .modal-right').slideToggle();
      setTimeout(function () {
        $('.single-car .blackInfo .btn-reservation .modal-body .modal-form ').slideToggle();
      }, 500);
    });

    $('.OpenModal').click(function () {
      $('.single-car .blackInfo .btn-reservation .modal-body .modal-left').css('display', 'block');
      $('.single-car .blackInfo .btn-reservation .modal-body .modal-center').css('display', 'flex');
      $('.single-car .blackInfo .btn-reservation .modal-body .modal-right').css('display', 'block');
      $('.single-car .blackInfo .btn-reservation .modal-body .modal-form ').css('display', 'none');
    })

    $('#myModal').on('shown.bs.modal', function () {
      $('#myInput').trigger('focus');
    })

    // variable URL GET
    $.urlParam = function (name) {
      var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
      if (results == null) {
        return null;
      } else {
        return decodeURI(results[1]) || 0;
      }
    }

    var pageCar = $.urlParam('pageCar');
    var gearBox = $.urlParam('gearBox');
    if ($('body').hasClass('page-template-singleCar')) {
      var parent = $('.single-car .blackInfo .nameCar').attr('id-parent')
      $('.rentcar .left-side').find('.' + parent).addClass('active');
    } else {
      if (pageCar == 0 || pageCar == null) {
        $('.rentcar .left-side').find('.single-car').first().addClass('active');
      } else {
        $('.rentcar .left-side').find('.' + pageCar).addClass('active');
      }
      if (gearBox == 0 || gearBox == null) {
        $('.rentcar .navCar').find('.option').first().addClass('active');
      } else {
        $('.rentcar .navCar').find('.' + gearBox).addClass('active');
      }
    }


    $('.rentcar .left-side .single-car').click(function () {
      if ($(this).hasClass('active')) {
        return false;
      } else {
        $('.rentcar .left-side').find('.active').removeClass('active');
        $(this).addClass('active');
        var pageCar = $(this).attr('page-id');
        $('.rentForm .pageCarForm').val(pageCar);
        $('.rentForm').submit();
      }
    });

    $('.rentcar .right-side .navCar .option').click(function () {
      if ($(this).hasClass('active')) {
        return false;
      } else {
        $('.right-side .navCar ').find('.active').removeClass('active');
        $(this).addClass('active');
        var gearBox = $(this).attr('gearBox');
        $('.rentForm .gearBoxForm').val(gearBox);
        $('.rentForm').submit();
      }
    });

    $('#sidebar .car-box .single-car').click(function () {
      if ($(this).hasClass('active')) {
        return false;
      } else {
        $('#sidebar .car-box ').find('.active').removeClass('active');
        $(this).addClass('active');
        var pageCar = $(this).attr('page-id');
        $('.rentForm .pageCarForm').val(pageCar);
      }
    });

    $('#sidebar .navCar .option').click(function () {
      if ($(this).hasClass('active')) {
        return false;
      } else {
        $('#sidebar .navCar ').find('.active').removeClass('active');
        $(this).addClass('active');
        var gearBox = $(this).attr('gearBox');
        $('.rentForm .gearBoxForm').val(gearBox);
      }
    });
    $('#sidebar .btn-container a').click(function (event) {
      event.preventDefault();
      $('.rentForm').submit();
    });

    // lightcase
    $('a[data-rel^=lightcase]').lightcase();
    // Animate Aos
    AOS.init({
      disable: 'mobile'
    });
    // End document.ready
  });
})(jQuery);