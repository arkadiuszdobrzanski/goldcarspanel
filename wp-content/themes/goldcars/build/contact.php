<?php
/* Template name: Kontakt */
?>
<?php get_header(); ?>

<div class="container-layout container-fluid single-page contact-page">
    <div class="row">
        <div class="left-side col-md-8 col-lg-8 col-xl-9">
			<?php if ( have_posts() ) : ?>
				<?php while ( have_posts() ) : the_post(); ?> 
				<?php 
                $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->post_parent), 'full' );
                if( empty( $thumb ) ) {
                    $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
                }
                ?>

                <div class="bg-page" style="background-image: url('<?php echo $thumb['0'];?>')">
                    <div class="headerPage">
                        <p data-aos="fade-down" data-aos-offset="300" data-aos-duration="2000" data-aos-delay="500" data-aos-once="true"><?php echo get_the_title( $post->post_parent ); ?></p>
                        <h3 data-aos="fade-down" data-aos-offset="300" data-aos-duration="2000" data-aos-delay="300" data-aos-once="true"><?php echo the_title(); ?></h3>
                        
                    </div>
				</div>
				<div class="content-page">
                    <div class="desc-page">
                        <?php the_content(); ?>
					</div>
					<div class="contact-form">
						<?php echo do_shortcode('[contact-form-7 id="5" title="Formularz 1"]'); ?>
					</div>
                </div>
				<?php endwhile; ?>
            <?php endif; ?>
            <?php 
                $title_map = get_field( 'title_map' );
            ?>
            <div class="mapTitle" data-aos="fade-down" data-aos-offset="300" data-aos-duration="2000" data-aos-delay="300" data-aos-once="true">
                <h3><?php echo $title_map ?></h3>
            </div>
			<div id="map">

			</div>
            
            <!-- End left side -->
        </div>
        <div id="sidebar" class="right-side col-md-4 col-lg-4 col-xl-3">
            <?php get_template_part('rightSide'); ?>
        </div>
    </div>
</div>
<script>
	function initMap() {
		var uluru = {lat: 50.2861155, lng: 18.6637322};

		var styledMapType = new google.maps.StyledMapType(
			[
				{
        "featureType": "water",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#e9e9e9"
            },
            {
                "lightness": 17
            }
        ]
    },
    {
        "featureType": "landscape",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#f5f5f5"
            },
            {
                "lightness": 20
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#ffffff"
            },
            {
                "lightness": 17
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "color": "#ffffff"
            },
            {
                "lightness": 29
            },
            {
                "weight": 0.2
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#ffffff"
            },
            {
                "lightness": 18
            }
        ]
    },
    {
        "featureType": "road.local",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#ffffff"
            },
            {
                "lightness": 16
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#f5f5f5"
            },
            {
                "lightness": 21
            }
        ]
    },
    {
        "featureType": "poi.park",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#dedede"
            },
            {
                "lightness": 21
            }
        ]
    },
    {
        "elementType": "labels.text.stroke",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "color": "#ffffff"
            },
            {
                "lightness": 16
            }
        ]
    },
    {
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "saturation": 36
            },
            {
                "color": "#333333"
            },
            {
                "lightness": 40
            }
        ]
    },
    {
        "elementType": "labels.icon",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "transit",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#f2f2f2"
            },
            {
                "lightness": 19
            }
        ]
    },
    {
        "featureType": "administrative",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#fefefe"
            },
            {
                "lightness": 20
            }
        ]
    },
    {
        "featureType": "administrative",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "color": "#fefefe"
            },
            {
                "lightness": 17
            },
            {
                "weight": 1.2
            }
        ]
    }
			],
					 {name: 'Styled Map'});

		var map = new google.maps.Map(document.getElementById('map'), {
			zoom: 10,
			center: uluru,
			disableDefaultUI: true
		});
		var image = {
    	    url: templateUrl+'/img/lokalizacja.png',
    		scaledSize: new google.maps.Size(38, 55) // scaled size
        };
		var marker = new google.maps.Marker({
			position: uluru,
			map: map,
    	    icon: image
		});
		var myCity = new google.maps.Circle({
    center: uluru,
    radius: 28000,
    strokeColor: "#0000FF",
    strokeOpacity: 0.8,
    strokeWeight: 0,
    fillColor: "#ffe30b",
    fillOpacity: 0.4
  });
  myCity.setMap(map);
		map.mapTypes.set('styled_map', styledMapType);
		map.setMapTypeId('styled_map');
	}
</script>
<script async defer
src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBPlg2JQFdOJEcJ5T0vamzQLyRm8gqyTKU&callback=initMap">
</script>
<?php get_footer(); ?>
