<?php
	$title_1 = get_field( 'title_1', 'option' );
	$title_2 = get_field( 'title_2', 'option' );
	$obslugiwane_obszary = get_field( 'obslugiwane_obszary', 'option' );
	$title_3 = get_field( 'title_3', 'option' );
	$contact = get_field( 'contact', 'option' );
?>
<div class="wrapper-footer">
  <div class="container-fluid">
    <div class="row">
      	<div class="col-md-4 column">
			<h2><?php echo $title_1 ?></h2>
			<?php $defaults = array(
				'theme_location'  => 'primary-menu',
				'container'       => false,
				'menu_class'      => 'footer-nav',
				'menu_id'         => '',
				'fallback_cb'     => 'wp_page_menu',
				'before'          => '',
				'after'           => '',
				'link_before'     => '',
				'link_after'      => '',
				'items_wrap'      => '<ul class="%2$s">%3$s</ul>',
			);
			wp_nav_menu( $defaults ); ?>
	  	</div>
	  	<div class="col-md-4 column">
		  <h2><?php echo $title_2 ?></h2>
		  <?php echo $obslugiwane_obszary ?>
		</div>
		<div class="col-md-4 column">
		<h2><?php echo $title_3 ?></h2>
		<?php echo $contact ?>
		<p>Projekt serwisu</p>
		<a class="weblider" target="_blank" href="http://www.weblider.eu">  <img src="<?php bloginfo('template_url'); ?>/img/weblider.png" alt="weblider"></a>
		</div>  
    </div>
  </div>
</div>
<div class="drawer-overlay drawer-toggle">

</div>
<!-- Form GET -->
<?php
    $pageCar=$_GET['pageCar'];
    $gearBox=$_GET['gearBox'];
?>
<?php 
		$post_id = 12; //specify post id here
		$post = get_post($post_id); 
		$slug = $post->post_name;
?>
<form class="rentForm" action="<?php bloginfo('url'); ?>/<?php echo $slug ?>/" method="GET">
    <input class="pageCarForm" type="text" name="pageCar" value="<?php echo $pageCar ?>">
    <input class="gearBoxForm" type="text" name="gearBox" value="<?php echo $gearBox ?>">
    <input class="submit" type="submit" value="Zobacz samochód">
</form>
    <!-- Form GET END-->
<?php wp_footer(); ?>
</body>
</html>
