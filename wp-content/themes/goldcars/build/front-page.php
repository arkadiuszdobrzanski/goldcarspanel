<?php
/* Template name: Front-page */
?>
<?php get_header(); ?>
<?php
    $background_front = get_field( 'background_front' );
    $desc_front = get_field( 'desc_front' );
    $coast_text = get_field( 'coast_text' );
    $value_text = get_field( 'value_text' );
    $text_mouse = get_field( 'text_mouse' );
?>
<div class="container-layout container-fluid">
    <div class="row">
        <div class="left-side col-md-8 col-lg-8 col-xl-9">
            <div class="top" style="background-image:url(<?php echo $background_front ?>)">
                <div class="row fullHeight">
                    <div class="col-12 col-sm-12 col-md-10 col-lg-9 col-xl-6  offset-md-1 offset-lg-2 offset-xl-6 textFront">
                        <div class="desc">
                            <div data-aos="fade-left" data-aos-offset="100" data-aos-duration="2000" data-aos-delay="300" data-aos-once="true">
                            <?php echo $desc_front ?>
                            </div>
                            <div class="znacznik" data-aos="fade-left" data-aos-offset="100" data-aos-duration="2000" data-aos-delay="500" data-aos-once="true">
                                <p><?php echo $coast_text ?></p>
                                <h3><?php echo $value_text ?></h3>
                            </div>
                            <div class="scroll-downs">
                                <div class="mousey">
                                    <div class="scroller"></div>
                                </div>
                                <p><?php echo $text_mouse ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="sidebar" class="right-side mobile">
                <?php get_template_part('rightSide'); ?>
            </div>
            <div class="offer">
                <div class="row">
                    <?php
                    $args = array(
                        'post_type'      => 'page',
                        'posts_per_page' => -1,
                        'post_parent'    => 14,
                        'order'          => 'ASC',
                        'orderby'        => 'menu_order'
                     );
                    $parent = new WP_Query( $args );
                    if ( $parent->have_posts() ) : $offerAnimate = 3; ?>
                        <?php while ( $parent->have_posts() ) : $parent->the_post(); $offerAnimate++; ?>
                        <?php 
                            $icon_page = get_field( 'icon_page' );
                        ?>
                            <div class="single-offer col-md-6 col-lg-6 col-xl-4" data-aos="fade-left" data-aos-offset="100" data-aos-duration="1500" data-aos-delay="<?php echo $offerAnimate ?>00" data-aos-once="true">
                                <a href="<?php the_permalink(); s?>">
                                    <div class="bg">
                                        <img src="<?php echo $icon_page ?>" alt="">
                                        <h3><?php echo the_title(); ?></h3>
                                        <div class="descOffer">
                                        <?php if ( ! has_excerpt() ) {
                                                echo '';
                                            } else { 
                                                the_excerpt();
                                            }
                                        ?>
                                        </div>
                                        <div class="btn-container">
                                            <p>Dowiedz się więcej</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        <?php endwhile; ?>
                    <?php endif; wp_reset_query(); ?>
                </div>
            </div>
            <?php 
                $background_staff = get_field( 'background_staff' );
                $title_staff = get_field( 'title_staff' );
            ?>
            <div class="obsluga" style="background-image:url(<?php echo $background_staff ?>)">
                <div class="header_title" data-aos="fade-down" data-aos-offset="300" data-aos-duration="1500" data-aos-delay="300" data-aos-once="true">
                    <h2><?php echo $title_staff ?></h2>
                </div>
                <div class="row">
                    <?php if( have_rows('box_field') ): $obslugaAnimate = 3;?>
                        <?php while( have_rows('box_field') ): the_row(); $obslugaAnimate++; 
                            $title_box = get_sub_field('title_box');
                            $desc_box = get_sub_field('desc_box');
                            ?>
                                <div class="single-box col-md-6 col-lg-4" data-aos="fade-up" data-aos-offset="300" data-aos-duration="1500" data-aos-delay="<?php echo $obslugaAnimate ?>00" data-aos-once="true">
                                    <h2><?php echo $title_box ?></h2>
                                    <div class="desc_box">
                                        <?php echo $desc_box ?>
                                    </div>
                                </div>
                        <?php endwhile; ?>
                    <?php endif; ?>
                </div>
            </div>
            <?php 
                $title_questions = get_field( 'title_questions' );
            ?>
            <div class="questions">
                <div class="header_title" data-aos="fade-down" data-aos-offset="100" data-aos-duration="1500" data-aos-delay="100" data-aos-once="true">
                    <h2><?php echo $title_questions ?></h2>
                </div>
                <?php
                $args = array(
                    'post_type'      => 'page',
                    'posts_per_page' => -1,
                    'post_parent'    => 18,
                    'order'          => 'ASC',
                    'orderby'        => 'menu_order'
                    );
                $parent = new WP_Query( $args );
                if ( $parent->have_posts() ) : $questionsAnimate = 2; ?>
                    <div class="row">
                        <?php while ( $parent->have_posts() ) : $parent->the_post(); $questionsAnimate++;?>
                        <div class="single_questions col-lg-6 col-xl-4" data-aos="fade-right" data-aos-offset="100" data-aos-duration="1500" data-aos-delay="<?php echo $questionsAnimate ?>00" data-aos-once="true">
                                <a href="<?php the_permalink(); ?>"><?php the_title() ?></a>
                        </div>
                        <?php endwhile; ?>
                    </div>
                <?php endif; wp_reset_query(); ?>
            </div>
            <?php 
                $title_map = get_field( 'title_map' );
            ?>
            <div class="googlemap">
                <div class="header_title" data-aos="fade-down" data-aos-offset="300" data-aos-duration="2000" data-aos-delay="300" data-aos-once="true">
                    <h2><?php echo $title_map ?></h2>
                </div>
                <div id="map">

                </div>
            </div>
            <!-- End left side -->
        </div>
        <div id="sidebar" class="right-side desktop col-md-4 col-lg-4 col-xl-3">
            <div id="sticky">
                <?php get_template_part('rightSide'); ?>
            </div>
        </div>
    </div>
</div>
<script>
	function initMap() {
		var uluru = {lat: 50.2861155, lng: 18.6637322};

		var styledMapType = new google.maps.StyledMapType(
			[
				{
        "featureType": "water",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#e9e9e9"
            },
            {
                "lightness": 17
            }
        ]
    },
    {
        "featureType": "landscape",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#f5f5f5"
            },
            {
                "lightness": 20
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#ffffff"
            },
            {
                "lightness": 17
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "color": "#ffffff"
            },
            {
                "lightness": 29
            },
            {
                "weight": 0.2
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#ffffff"
            },
            {
                "lightness": 18
            }
        ]
    },
    {
        "featureType": "road.local",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#ffffff"
            },
            {
                "lightness": 16
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#f5f5f5"
            },
            {
                "lightness": 21
            }
        ]
    },
    {
        "featureType": "poi.park",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#dedede"
            },
            {
                "lightness": 21
            }
        ]
    },
    {
        "elementType": "labels.text.stroke",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "color": "#ffffff"
            },
            {
                "lightness": 16
            }
        ]
    },
    {
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "saturation": 36
            },
            {
                "color": "#333333"
            },
            {
                "lightness": 40
            }
        ]
    },
    {
        "elementType": "labels.icon",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "transit",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#f2f2f2"
            },
            {
                "lightness": 19
            }
        ]
    },
    {
        "featureType": "administrative",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#fefefe"
            },
            {
                "lightness": 20
            }
        ]
    },
    {
        "featureType": "administrative",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "color": "#fefefe"
            },
            {
                "lightness": 17
            },
            {
                "weight": 1.2
            }
        ]
    }
			],
					 {name: 'Styled Map'});

		var map = new google.maps.Map(document.getElementById('map'), {
			zoom: 10,
			center: uluru,
			disableDefaultUI: true
		});
		var image = {
    	    url: templateUrl+'/img/lokalizacja.png',
    		scaledSize: new google.maps.Size(38, 55) // scaled size
        };
		var marker = new google.maps.Marker({
			position: uluru,
			map: map,
    	    icon: image
		});
		var myCity = new google.maps.Circle({
    center: uluru,
    radius: 28000,
    strokeColor: "#0000FF",
    strokeOpacity: 0.8,
    strokeWeight: 0,
    fillColor: "#ffe30b",
    fillOpacity: 0.4
  });
  myCity.setMap(map);
		map.mapTypes.set('styled_map', styledMapType);
		map.setMapTypeId('styled_map');
	}
</script>
<script async defer
src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBPlg2JQFdOJEcJ5T0vamzQLyRm8gqyTKU&callback=initMap">
</script>
<?php get_footer(); ?>
