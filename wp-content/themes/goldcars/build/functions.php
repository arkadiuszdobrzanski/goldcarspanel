<?php
add_theme_support( 'post-thumbnails' );
add_post_type_support( 'page', 'excerpt' );
// Active menu

@ini_set( 'upload_max_size' , '64M' );
@ini_set( 'post_max_size', '64M');
@ini_set( 'max_execution_time', '300' );
//Register Navigations
add_action( 'init', 'my_custom_menus' );
function my_custom_menus() {
    register_nav_menus(
        array(
            'primary-menu' => __( 'Primary Menu' ),
            'secondary-menu' => __( 'Secondary Menu' ),
            'Lang' => __( 'Języki' )
        )
    );
}
// Stylesheet and Script

function style_script() {
  wp_enqueue_style( 'plugin-css', get_template_directory_uri() . '/dist/css/plugin.min.css' );
  wp_enqueue_style( 'style', get_template_directory_uri() . '/dist/css/main.css', '', time() );
  wp_enqueue_style( 'googlefonts', 'https://fonts.googleapis.com/css?family=Asap:400,500,700' );
    // Scripts
  wp_deregister_script('jquery-script');
    wp_deregister_script('jquery');
    wp_register_script('jquery', '//code.jquery.com/jquery-3.2.1.min.js', array(), null);
    wp_enqueue_script('jquery');

  wp_enqueue_script( 'script', get_template_directory_uri() . '/dist/js/scripts.js', array( 'jquery' ), time(), true );
  wp_enqueue_script( 'plugins', get_template_directory_uri() . '/dist/js/plugin.min.js', array( 'jquery' ), time(), true );
  wp_enqueue_script( 'bootstrap-js', get_template_directory_uri() . '/dist/js/javascripts/bootstrap.min.js', array( 'jquery' ), time(), true );
}
add_action( 'wp_enqueue_scripts', 'style_script' );


if( function_exists('acf_add_options_page') ) {
	acf_add_options_page(array(
		'page_title' 	=> 'Header',
		'menu_title'	=> 'Nagłówek',
		'menu_slug' 	=> 'header',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
		));
		acf_add_options_page(array(
		'page_title' 	=> 'Footer',
		'menu_title'	=> 'Stopka',
		'menu_slug' 	=> 'footer',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
    ));
    acf_add_options_page(array(
      'page_title' 	=> 'Modal',
      'menu_title'	=> 'Modal',
      'menu_slug' 	=> 'modal',
      'capability'	=> 'edit_posts',
      'redirect'		=> false
      ));

}
// require get_template_directory().'/inc/ajax.php';
require get_template_directory().'/inc/pagination.php';



// Skróty
function tempDir() {
    return get_template_directory_uri();
}

function imageDir( $string ) {
    return tempDir() . '/img/' . $string;
}


function fileDir() {
    return get_template_directory();
}
/**
 *  Podpinanie lightcase pod galerie
 */

    function get_id($inc = false) {
      static $id;
        if ($inc) {
          $id++;
        }
      return $id;
    }

    function replace($link) {
      $id = get_id();
      return str_replace('<a href=', '<a data-rel="lightcase:Gallery-'.$id.'" href=', $link);
    }

    add_filter(
      'post_gallery',
      function() {
        get_id(true);
        add_filter('wp_get_attachment_link','replace');
      }
    );

    function taco_gallery_default_settings( $settings ) {
    $settings['galleryDefaults']['link'] = 'file';
    return $settings;
}
add_filter( 'media_view_settings', 'taco_gallery_default_settings');
