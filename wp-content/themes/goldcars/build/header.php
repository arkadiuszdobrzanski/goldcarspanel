<!DOCTYPE HTML>
<html <?php language_attributes(); ?> class="no-js">
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <title><?php bloginfo('name'); ?> <?php if ( is_single() || is_page() ) { ?> - <?php the_title(); } ?> </title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="format-detection" content="telephone=no">
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo get_template_directory_uri(); ?>/img/favicon/favicon.ico" />
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo get_template_directory_uri(); ?>/img/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo get_template_directory_uri(); ?>/img/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_template_directory_uri(); ?>/img/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_template_directory_uri(); ?>/img/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_template_directory_uri(); ?>/img/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo get_template_directory_uri(); ?>/img/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_template_directory_uri(); ?>/img/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo get_template_directory_uri(); ?>/img/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/img/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo get_template_directory_uri(); ?>/img/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri(); ?>/img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo get_template_directory_uri(); ?>/img/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri(); ?>/img/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/img/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/img/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <script type="text/javascript">
      var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
      var templateUrl = '<?= get_bloginfo("template_url"); ?>';
    </script>
       <!--[if lt IE 9]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <?php wp_head();?>
</head>
<body <?php body_class('drawer drawer--right'); ?>>
<?php 
  global $post;

  if ($post->post_parent == 12 || is_page(12) || is_page_template( 'singleCar.php' )) {
      $class = "carPage";
      $displayColumn = "display";
      $column = 'col-12';
  } else {
    $column = 'col-md-8 col-lg-8 col-xl-9';
    $displayColumn = "col-md-4 col-lg-4 col-xl-3";
  }
?>
<div class="header <?php echo $class ?>">
  <div class="container-fluid">
    <div class="row">
      <div class="left-side <?php echo $column ?>">
        <div class="row">
            <?php
            $logo_header = get_field( 'logo_header', 'option' );
            $phone_header = get_field( 'phone_header', 'option' );
            $phone_header_2 = get_field( 'phone_header_2', 'option' );
            // $adress_header = get_field( 'adress_header', 'option' );
            // $link_google_map = get_field( 'link_google_map', 'option' );
          ?>
          <div class="header__logo col-6 col-sm-5 col-md-5 col-lg-4">
              <a href="<?php echo esc_url( home_url( '/' ) ); ?>">
                  <img src="<?php esc_html_e( $logo_header ); ?>" alt="logo">
              </a>
          </div>
          <div class="col-6 col-sm-7 col-md-7 col-lg-8 header__navigation">
            <div class="tel">
              <a href="tel:<?php echo $phone_header ?>">
                <img class="white" src="<?php echo get_bloginfo( 'template_directory' ); ?>/img/ikona-telefon@2.png" />
                <img class="black" src="<?php echo get_bloginfo( 'template_directory' ); ?>/img/ikona-telefon-ciemne@2.png" />
                  <p><?php echo $phone_header  ?></p>
                </a>
            </div>
            <div class="tel">
              <a href="tel:<?php echo $phone_header_2 ?>">
                <img class="white" src="<?php echo get_bloginfo( 'template_directory' ); ?>/img/ikona-telefon@2.png" />
                <img class="black" src="<?php echo get_bloginfo( 'template_directory' ); ?>/img/ikona-telefon-ciemne@2.png" />
                  <p><?php echo $phone_header_2  ?></p>
                </a>
            </div>
            <!-- <div class="adress">
              <a href="<?php echo $link_google_map ?>">
                <img class="white" src="<?php echo get_bloginfo( 'template_directory' ); ?>/img/ikona-lokalizacja@2.png" />
                <img class="black" src="<?php echo get_bloginfo( 'template_directory' ); ?>/img/ikona-lokalizacja-ciemne@2.png" />
                <p><?php echo $adress_header  ?></p>
              </a>
            </div> -->
            <div class="drawer drawer--right">
              <div class="buttonDrawer">
                <p class="buttonText">Menu</p>
                <button type="button" class="drawer-toggle drawer-hamburger">
                  <span class="sr-only">toggle navigation</span>
                  <span class="drawer-hamburger-icon"></span>
                </button>
              </div>
              <nav class="drawer-nav" role="navigation">
              <ul class="drawer-menu">
                <div class="nav-position">
                  <?php $defaults = array(
                    'theme_location'  => 'primary-menu',
                    'container'       => false,
                    'menu_class'      => 'nav-menu mobile-menu',
                    'menu_id'         => '',
                    'fallback_cb'     => 'wp_page_menu',
                    'before'          => '',
                    'after'           => '',
                    'link_before'     => '',
                    'link_after'      => '',
                    'items_wrap'      => '<ul class="%2$s">%3$s</ul>',
                  );
                  wp_nav_menu( $defaults ); ?>
                  </div>
              </ul>
              </nav>
            </div>
          </div>
        </div>
      </div>
      <div class="right-side <?php echo $displayColumn ?>">
      </div>
    </div>
  </div>
</div>
