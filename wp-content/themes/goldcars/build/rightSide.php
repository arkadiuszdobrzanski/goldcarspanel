
    <div class="headerSide">
        <h3>Wybierz samochód</h3>
    </div>
    <div class="titleBox">
        <p>Typ samochodu</p>
    </div>
    <div class="car-box">
        <div class="row">
            <?php
            $args = array(
                'post_type'      => 'page',
                'posts_per_page' => -1,
                'post_parent'    => 12,
                'order'          => 'ASC',
                'orderby'        => 'menu_order'
            );
            $parent = new WP_Query( $args );
            if ( $parent->have_posts() ) : ?>
                <?php while ( $parent->have_posts() ) : $parent->the_post(); ?>
                    <div class="single-car col-6" page-id="<?php echo get_the_ID() ?>">
                        <div class="bg">
                            <?php 
                                if ( has_post_thumbnail() ) {
                                    the_post_thumbnail();
                                } 
                            ?>
                            <h3><?php the_title(); ?></h3>
                        </div>
                    </div>                            
                <?php endwhile; ?>
            <?php endif; wp_reset_query(); ?>
        </div>
    </div>
    <div class="titleBox">
        <p>Skrzynia biegów</p>
    </div>
    <div class="navCar row">
        <div class="option dowolna col-4" gearBox="dowolna">
            <div class="bg">
                <img src="<?php echo get_bloginfo( 'template_directory' ); ?>/img/ikona-skrzynia-dowolna-jasna@2.png" />
                <p>Dowolna</p>
            </div>
        </div>
        <div class="option automatyczna col-4" gearBox="automatyczna">
            <div class="bg">
                <img src="<?php echo get_bloginfo( 'template_directory' ); ?>/img/ikona-skrzynia-automatyczna-jasna@2.png" />
                <p>Automatyczna</p>
            </div>
        </div>
        <div class="option manualna col-4" gearBox="manualna">
            <div class="bg">
                <img src="<?php echo get_bloginfo( 'template_directory' ); ?>/img/ikona-skrzynia-manualna-jasna@2.png" />
                <p>Manualna</p>
            </div>
        </div>
    </div>
    <div class="btn-container">
        <a href="">Zobacz samochód</a>
    </div>



