<?php get_header(); ?>
<?php
    $background_front = get_field( 'background_front' );
    $desc_front = get_field( 'desc_front' );
    $coast_text = get_field( 'coast_text' );
    $value_text = get_field( 'value_text' );
?>
<div class="container-layout container-fluid single-page">
    <div class="row">
        <div class="left-side col-md-8 col-lg-8 col-xl-9">
            <?php 
            global $post;

            ?>
            <?php if ( have_posts() ) : ?>
                <?php while ( have_posts() ) : the_post(); ?> 
                <?php 
                $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->post_parent), 'full' );
                if( empty( $thumb ) ) {
                    $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
                }
                ?>

                <div class="bg-page" style="background-image: url('<?php echo $thumb['0'];?>')">
                    <div class="headerPage" >
                        <p data-aos="fade-down" data-aos-offset="300" data-aos-duration="2000" data-aos-delay="500" data-aos-once="true"><?php echo get_the_title( $post->post_parent ); ?></p>
                        <h3 data-aos="fade-down" data-aos-offset="300" data-aos-duration="2000" data-aos-delay="300" data-aos-once="true"><?php echo the_title(); ?></h3>
                        
                    </div>
                </div>
                <div class="content-page">
                    <div class="desc-page">
                        <?php the_content(); ?>
                    </div>
                </div>
                <?php endwhile; ?>
            <?php endif; ?>
            
            <!-- End left side -->
        </div>
        <div id="sidebar" class="right-side col-md-4 col-lg-4 col-xl-3">
            <?php get_template_part('rightSide'); ?>
        </div>
    </div>
</div>

<?php get_footer(); ?>
