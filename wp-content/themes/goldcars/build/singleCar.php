<?php
/* Template name: Pojedyńczy samochód */
?>
<?php get_header(); ?>
<div class="rentcar">
    <div class="container-fluid">
        <div class="row">
            <div class="left-side singlePage col-md-2">
                <div class="headerTitle">
                    <p>Typ samochodu</p>
                </div>
                <?php
                $args = array(
                    'post_type'      => 'page',
                    'posts_per_page' => -1,
                    'post_parent'    => 12,
                    'order'          => 'ASC',
                    'orderby'        => 'menu_order'
                );
                $parent = new WP_Query( $args );
                if ( $parent->have_posts() ) : $kategoria = 3;?>
                    <div class="flexContainer">
                    <div class="single-car btn-all" page-id="" data-aos="fade-up" data-aos-offset="100" data-aos-duration="1500" data-aos-delay="<?php echo $kategoria ?>00" data-aos-once="true">
                    	    <div class="bg">
								<div class="btn-container">
									<a href="#">Wszystkie</a>
								</div>
                    	    </div>
                    	</div> 
                        <?php while ( $parent->have_posts() ) : $parent->the_post(); $kategoria++; ?>
                            <div class="single-car <?php echo get_the_ID() ?>" page-id="<?php echo get_the_ID() ?>" data-aos="fade-up" data-aos-offset="100" data-aos-duration="1500" data-aos-delay="<?php echo $kategoria ?>00" data-aos-once="true">
                                <div class="bg">
                                    <?php
                                        if ( has_post_thumbnail() ) {
                                            the_post_thumbnail();
                                        }
                                    ?>
                                    <h3><?php the_title(); ?></h3>
                                </div>
                            </div>
                        <?php endwhile; ?>
                    </div>
                <?php endif; wp_reset_query(); ?>
            </div>
            <div class="right-side single-car col-md-10">
                <div class="row fullHeight">
                    <div class="blackInfo col-lg-4">
                        <?php
                            $text_back = get_field( 'text_back' );
                        ?>
                        <div class="btn-back">
                            <a href="<?php echo get_page_link(12); ?>"><?php echo $text_back ?></a>
                        </div>
                        <?php if ( have_posts() ) : ?>
                            <?php while ( have_posts() ) : the_post(); ?>
                            <?php
                           		$desc_car = get_field( 'desc_car' );
                                $km_car = get_field( 'km_car' );
                                $gearbox_car = get_field( 'gearbox_car' );
                                $fuel_car = get_field( 'fuel_car' );
                                $combustion_car = get_field( 'combustion_car' );
                                $engine_car = get_field( 'engine_car' );
                                $seats_car = get_field( 'seats_car' );
                                $title_rent = get_field( 'title_rent' );
                                $title_equipment = get_field( 'title_equipment' );
                                $phone_header = get_field( 'phone_header', 'option' );
                                $title_modal = get_field( 'title_modal', 'option' );
                                $icon_1 = get_field( 'icon_1', 'option' );
                                $text_1 = get_field( 'text_1', 'option' );
                                $icon_2 = get_field( 'icon_2', 'option' );
                            ?>
                                <div class="nameCar" id-parent="<?php echo wp_get_post_parent_id( $post_ID );?>">
                                    <h3><?php echo the_title(); ?></h3>
                                    <?php echo $desc_car ?>
                                </div>
                                <div class="btn-reservation">
                                    <!-- Button trigger modal -->
                                    <button type="button" class="OpenModal btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">
                                    Zarezerwuj
                                    </button>

                                    <!-- Modal -->
                                    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                            <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                  <span aria-hidden="true">&times;</span>
                                                </button>
                                                <h5 class="modal-title" id="exampleModalLongTitle"><?php echo $title_modal ?></h5>
                                                <p><?php echo the_title(); ?></p>
                                            </div>
                                            <div class="modal-body">
                                                <div class="flexContainer">
                                                    <div class="modal-left">
                                                        <img src="<?php echo $icon_1 ?>" />
                                                        <h3><?php echo $text_1 ?></h3>
                                                        <a href="tel:<?php echo $phone_header ?>"><?php echo $phone_header ?></a>
                                                    </div>
                                                    <div class="modal-center">
                                                        <p>lub</p>
                                                    </div>
                                                    <div class="modal-right">
                                                        <img src="<?php echo $icon_2 ?>" />
                                                        <a href="#">Napisz do nas</a>
                                                    </div>
                                                    <div class="modal-form">
                                                        <?php echo do_shortcode('[contact-form-7 id="201" title="Napisz do nas"]'); ?>
                                                    </div>
                                                </div>
                                            </div>

                                    </div>
                                </div>
                            </div>
                                </div>
                                <div class="infoCar">
                                    <?php
                                        if( !empty( $km_car ) ) { ?>
                                            <div class="optionCar">
                                                <p>Moc silnika: <?php echo $km_car?>km </p>
                                            </div>
                                        <?php }
                                    ?>
                                    <?php
                                        if( !empty( $engine_car ) ) { ?>
                                            <div class="optionCar">
                                                <p>Silnik: <?php echo $engine_car ?></p>
                                            </div>
                                        <?php }
                                    ?>
                                    <?php
                                        if( !empty( $combustion_car ) ) { ?>
                                            <div class="optionCar">
                                                <p>Spalanie: <?php echo $combustion_car ?>l/100km</p>
                                            </div>
                                        <?php }
                                    ?>
                                    <?php
                                        if( !empty( $gearbox_car ) ) { ?>
                                            <div class="optionCar">
                                                <p>Skrzynia <?php echo $gearbox_car ?></p>
                                            </div>
                                        <?php }
                                    ?>
                                    <?php
                                        if( !empty( $fuel_car ) ) { ?>
                                            <div class="optionCar">
                                                <p>Paliwo: <?php echo $fuel_car ?></p>
                                            </div>
                                        <?php }
                                    ?>
                                        <?php
                                            if( !empty( $seats_car ) ) { ?>
                                                <div class="optionCar">
                                                    <p>Ilość miejsc: <?php echo $seats_car ?></p>
                                                </div>
                                            <?php }
                                        ?>
                                </div>
                                <div class="rentTime">
                                    <?php
                                        if( !empty( $title_rent ) ) { ?>
                                            <div class="headerRent">
                                                <h3><?php echo $title_rent ?></h3>
                                            </div>
                                        <?php }
                                    ?>
                                    <div class="boxRent">
                                        <?php if( have_rows('coast_hours') ): ?>
											<div class="netto">
												<p>Podana kwota jest kwotą Netto</p>
											</div>
                                            <?php while( have_rows('coast_hours') ): the_row();
                                                $time_rent = get_sub_field('time_rent');
                                                $coast_rent = get_sub_field('coast_rent');
                                                ?>
                                                <div class="rentInfo">
                                                    <div class="time">
                                                        <p><?php echo $time_rent ?></p>
                                                    </div>
                                                    <div class="coast">
                                                        <p><?php echo $coast_rent ?></p>
                                                    </div>
                                                </div>
                                            <?php endwhile; ?>

                                        <?php endif; ?>
                                    </div>
                                </div>
                            <?php endwhile; ?>
                        <?php endif; ?>
                    </div>
                    <div class="carThumb col-lg-8">
                        <div class="thumb" data-aos="fade-down" data-aos-offset="300" data-aos-duration="1500" data-aos-delay="300" data-aos-once="true">
                            <?php
                                if ( has_post_thumbnail() ) {
                                    the_post_thumbnail();
                                }
                            ?>
                        </div>
                        <div class="equipment">
                            <div class="headerEquipment" data-aos="fade-right" data-aos-duration="1500" data-aos-delay="300" data-aos-once="true">
                                <h3><?php echo $title_equipment ?></h3>
                            </div>
                                <?php if( have_rows('equipment_car') ):?>
                                    <div class="row">
                                        <?php while( have_rows('equipment_car') ): the_row();
                                            $equipment_single = get_sub_field('equipment_single');
                                            ?>
                                                <div class="equipmentSingle col-sm-6">
                                                    <p><?php echo $equipment_single ?></p>
                                                </div>
                                        <?php endwhile; ?>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>
