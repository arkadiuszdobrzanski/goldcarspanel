<?php
/* Template name: Samochody na wynajem */
?>
<?php get_header(); ?>
<?php
    $pageCar=$_GET['pageCar'];
    $gearBox=$_GET['gearBox'];
?>
<div class="rentcar">
    <div class="container-fluid">
        <div class="row">
            <div class="left-side col-md-2">
                <div class="headerTitle">
                    <p>Typ samochodu</p>
                </div>
                <?php
                $args = array(
                    'post_type'      => 'page',
                    'posts_per_page' => -1,
                    'post_parent'    => 12,
                    'order'          => 'ASC',
                    'orderby'        => 'menu_order'
                );
                $parent = new WP_Query( $args );
                if ( $parent->have_posts() ) : $kategoria = 3;?>
                    <div class="flexContainer">
                    	<div class="single-car btn-all" page-id="" data-aos="fade-up" data-aos-offset="100" data-aos-duration="1500" data-aos-delay="<?php echo $kategoria ?>00" data-aos-once="true">
                    	    <div class="bg">
								<div class="btn-container">
									<a href="#">Wszystkie</a>
								</div>
                    	    </div>
                    	</div> 
                        <?php while ( $parent->have_posts() ) : $parent->the_post(); $kategoria++;?>
                        <div class="single-car <?php echo get_the_ID() ?>" page-id="<?php echo get_the_ID() ?>" data-aos="fade-up" data-aos-offset="100" data-aos-duration="1500" data-aos-delay="<?php echo $kategoria ?>00" data-aos-once="true">
                            <div class="bg">
                                <?php 
                                    if ( has_post_thumbnail() ) {
                                        the_post_thumbnail();
                                    } 
                                ?>
                                <h3><?php the_title(); ?></h3>
                            </div>
                        </div> 
                        <?php endwhile; ?>
                    </div>
                <?php endif; wp_reset_query(); ?>
            </div>
            <div class="right-side col-md-10">
                <div class="navCar">
                    <div class="textOption">
                        <p>Skrzynia biegów</p>
                    </div>
                    <div class="option dowolna" gearBox="dowolna">
                        <p>Dowolna</p>
                    </div>
                    <div class="option automatyczna" gearBox="automatyczna">
                        <p>Automatyczna</p>
                    </div>
                    <div class="option manualna" gearBox="manualna">
                        <p>Manualna</p>
                    </div>
                </div>
                <div class="loopCar">
                    <?php 
                        if( !empty( $pageCar ) ) {
                            $page = $pageCar;
                        } else {
                            $page = [34, 38, 36, 40];
                        }
                        if( !empty( $gearBox ) ) {

                            if( $gearBox == 'dowolna' ) {
                                $key = '';
                            } else {
                                $key = $gearBox;
                            }
                            
                        } else {
                            $key = '';
                        }
                    ?>
                    <?php
                    $args = array(
                        'post_type'      => 'page',
                        'posts_per_page' => -1,
                        'post_parent'    => $page,
                        'order'          => 'ASC',
                        'orderby'        => 'menu_order',
                        'meta_key'		=> 'gearbox_car',
                        'meta_value'	=> $key
                     );
                    $parent = new WP_Query( $args );
                    if ( $parent->have_posts() ) : ?>
                        <div class="row">
                            <?php while ( $parent->have_posts() ) : $parent->the_post(); ?>
                            <?php 
                                $price_car = get_field( 'price_car' );
                                $km_car = get_field( 'km_car' );
                                $gearbox_car = get_field( 'gearbox_car' );
                                $fuel_car = get_field( 'fuel_car' );
                                $seats_car = get_field( 'seats_car' );
                            ?>
                                <div class="single-car col-lg-6 col-xl-4" data-aos="fade-up" data-aos-duration="1500" data-aos-delay="300" data-aos-once="true">
                                    <a href="<?php the_permalink(); ?>">
                                        <div class="bg">
                                            <?php 
                                                if ( has_post_thumbnail() ) {
                                                    the_post_thumbnail();
                                                } 
                                            ?>
                                            <h3><?php the_title(); ?></h3>
                                            <div class="desc">
                                                <?php 
                                                    if( !empty( $km_car ) ) { ?>
                                                        <div class="optionCar">
                                                            <p><?php echo $km_car ?> km</p>
                                                        </div>
                                                    <?php }
                                                     if( !empty( $gearbox_car ) ) { ?>
                                                    <div class="optionCar">
                                                        <p>skrzynia <?php echo $gearbox_car ?></p>
                                                    </div>
                                                    <?php }
                                                    if( !empty( $fuel_car ) ) { ?>
                                                        <div class="optionCar">
                                                            <p><?php echo $fuel_car ?></p>
                                                        </div>
                                                    <?php }
                                                    if( !empty( $seats_car  ) ) { ?>
                                                        <div class="optionCar">
                                                            <p><?php echo $seats_car ?> miejsc</p>
                                                        </div>
                                                    <?php } ?>
                                            </div>
                                            <?php
                                                if( !empty( $price_car ) ) { ?>
                                                    <div class="price">
                                                        <span>od:</span>
                                                        <p><?php echo $price_car ?> zł</p>
                                                    </div>
                                                <?php } 
                                            ?>
   
                                        </div>
                                    </a>
                                </div> 
                            <?php endwhile; ?>
                            <?php else : ?>
                            <div class="alert alert-secondary" role="alert"  data-aos="fade-up" data-aos-duration="1500" data-aos-delay="300" data-aos-once="true">
                                <p style="margin-bottom: 0px;"><?php _e( 'Brak samochodów o wybranych kryteriach.' ); ?></p>
                            </div>
                        </div>
                    <?php endif; wp_reset_query(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>
