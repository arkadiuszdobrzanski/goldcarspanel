jQuery( document ).ready(function( $ ) {

    "use strict";
   
    $(".gantt").gantt({
        source: "../wp-content/themes/goldcars/source.php",
        //source: "../wp-content/themes/goldcars/source.json",
        navigate: "scroll",
        maxScale: "days",
        itemsPerPage: 30,
        useCookie: true,
        dow: ["Nn", "Pn", "Wt", "Śr", "Cz", "Pt", "Sb"],
        months: ["Styczeń", "Luty", "Marzec", "Kwiecień", "Maj", "Czerwiec", "Lipiec", "Sierpień", "Wrzesień", "Październik", "Listopad", "Grudzień"],
        onItemClick: function(data) {
            window.location.replace("/wp-admin/post.php?post=" + data + "&action=edit");
        },
        onAddClick: function(dt, rowId) {
            window.location.replace("/wp-admin/post-new.php?post_type=wynajem");
        },
        onDataLoadFailed: function(data) {
            alert("Data failed to load.")
        }
    });

});