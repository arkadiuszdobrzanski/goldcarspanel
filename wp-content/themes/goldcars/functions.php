<?php
add_theme_support( 'post-thumbnails' );
add_post_type_support( 'page', 'excerpt' );


/* Usuwanie informacji o wersji z js i css */
function bezzo_remove_wp_version_strings( $src ) {
	global $wp_version;
	parse_str( parse_url($src, PHP_URL_QUERY), $query );
	if ( !empty( $query['ver'] ) && $query['ver'] === $wp_version ) {
		$src = remove_query_arg( 'ver', $src );
	}
	return $src;
}
add_filter( 'script_loader_src', 'bezzo_remove_wp_version_strings' );
add_filter( 'style_loader_src', 'bezzo_remove_wp_version_strings' );

/* Remove 'text/css' from our enqueued stylesheet */
function bezzo_style_remove( $tag ) {
    return preg_replace( '~\s+type=["\'][^"\']++["\']~', '', $tag );
}
add_filter( 'style_loader_tag', 'bezzo_style_remove' );


function bezzo_disable_emojicons_tinymce( $plugins ) {
    if ( is_array( $plugins ) ) {
        return array_diff( $plugins, array( 'wpemoji' ) );
    } else {
        return array();
    }
}


/* Wyłaczanie emoji */
function taco_disable_wp_emoji() {
    remove_action( 'admin_print_styles', 'print_emoji_styles' );
    remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
    remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
    remove_action( 'wp_print_styles', 'print_emoji_styles' );
    remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
    remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
    remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
    add_filter( 'tiny_mce_plugins', 'bezzo_disable_emojicons_tinymce' );
}
add_action( 'init', 'taco_disable_wp_emoji' );


/* Czyszczenie nagłówka */
function taco_wp_clear_head() {
	remove_action( 'wp_head', 'feed_links_extra', 3 );
	remove_action( 'wp_head', 'feed_links', 2 );
	remove_action( 'wp_head', 'rsd_link' );
	remove_action( 'wp_head', 'wlwmanifest_link' );
	remove_action( 'wp_head', 'index_rel_link' );
	remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 );
	remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );
    remove_action( 'wp_head', 'wp_shortlink_wp_head', 10, 0 );
	remove_action( 'wp_head', 'adjacent_posts_rel_link', 10, 0 );
	remove_action( 'wp_head', 'wp_generator' );
    remove_action( 'wp_head', 'rest_output_link_wp_head', 10 );
    remove_action( 'wp_head', 'wp_oembed_add_discovery_links', 10 );
    remove_action( 'xmlrpc_rsd_apis', 'rest_output_rsd' );
    remove_action( 'wp_head', 'rest_output_link_wp_head', 10 );
    remove_action( 'template_redirect', 'rest_output_link_header', 11 );
    remove_action( 'wp_head', 'wp_oembed_add_host_js' );
    remove_action('welcome_panel', 'wp_welcome_panel');
}
add_action('init', 'taco_wp_clear_head');


// Disable support for comments and trackbacks in post types
function bezzo_disable_comments_post_types_support() {
	$post_types = get_post_types();
	foreach ($post_types as $post_type) {
		if(post_type_supports($post_type, 'comments')) {
			remove_post_type_support($post_type, 'comments');
			remove_post_type_support($post_type, 'trackbacks');
		}
	}
}
add_action('admin_init', 'bezzo_disable_comments_post_types_support');


// Close comments on the front-end
function bezzo_disable_comments_status() {
	return false;
}
add_filter('comments_open', 'bezzo_disable_comments_status', 20, 2);
add_filter('pings_open', 'bezzo_disable_comments_status', 20, 2);


// Hide existing comments
function bezzo_disable_comments_hide_existing_comments($comments) {
	$comments = array();
	return $comments;
}
add_filter('comments_array', 'bezzo_disable_comments_hide_existing_comments', 10, 2);


// Remove comments page in menu
function bezzo_disable_comments_admin_menu() {
	remove_menu_page('edit-comments.php');
}
add_action('admin_menu', 'bezzo_disable_comments_admin_menu');


// Redirect any user trying to access comments page
function bezzo_disable_comments_admin_menu_redirect() {
	global $pagenow;
	if ($pagenow === 'edit-comments.php') {
		wp_redirect(admin_url()); exit;
	}
}
add_action('admin_init', 'bezzo_disable_comments_admin_menu_redirect');


// Remove comments metabox from dashboard
function bezzo_disable_comments_dashboard() {
	remove_meta_box('dashboard_recent_comments', 'dashboard', 'normal');
}
add_action('admin_init', 'bezzo_disable_comments_dashboard');


// Remove comments links from admin bar
function bezzo_disable_comments_admin_bar() {
	if (is_admin_bar_showing()) {
		remove_action('admin_bar_menu', 'wp_admin_bar_comments_menu', 60);
	}
}
add_action('init', 'bezzo_disable_comments_admin_bar');


// Disable displaying users in rest API
add_filter( 'rest_endpoints', function( $endpoints ){
    if ( isset( $endpoints['/wp/v2/users'] ) ) {
        unset( $endpoints['/wp/v2/users'] );
    }
    if ( isset( $endpoints['/wp/v2/users/(?P<id>[\d]+)'] ) ) {
        unset( $endpoints['/wp/v2/users/(?P<id>[\d]+)'] );
    }
    return $endpoints;
});


// Wyłączanie xmlrpc
add_filter( 'xmlrpc_enabled', '__return_false' );
add_filter( 'wp_headers', 'bezzo_remove_x_pingback' );
function bezzo_remove_x_pingback( $headers ) {
    unset( $headers['X-Pingback'] );
    return $headers;
}


// Wyłączanie ping
add_filter( 'xmlrpc_methods', 'bezzo_remove_ping' );
function bezzo_remove_ping( $methods ) {
   unset( $methods['pingback.ping'] );
   unset( $methods['pingback.extensions.getPingbacks'] );
   return $methods;
}


add_filter( 'wp_headers', 'bezzo_remove_pingback_headers' );
function bezzo_remove_pingback_headers( $headers ) {
   unset( $headers['X-Pingback'] );
   return $headers;
}

// Ukrywanie niepotrzebnych elementów menu na zapleczu
function remove_menu() 
{ 
  remove_menu_page('edit.php?post_type=page');
  remove_menu_page('edit.php');
  remove_menu_page('upload.php');
  remove_menu_page('themes.php');
  remove_menu_page('tools.php');
} 
add_action('admin_menu', 'remove_menu');








// 
//  Samochód 
//

// Add the custom columns to the samochod post type:
add_filter( 'manage_samochod_posts_columns', 'set_custom_edit_samochod_columns' );
function set_custom_edit_samochod_columns($columns) {
    unset( $columns['date'] );
    $columns['nr_rejestracyjny'] = __( 'Nr. rejestracyjny', 'your_text_domain' );
    $columns['cena_od'] = __( 'Cena od', 'your_text_domain' );
    $columns['rodzaj'] = __( 'Rodzaj', 'your_text_domain' );
    $columns['date'];
    return $columns;
}

// Add the data to the custom columns for the samochod post type:
  add_action( 'manage_samochod_posts_custom_column' , 'custom_samochod_column', 10, 2 );
  function custom_samochod_column( $column, $post_id ) {
      switch ( $column ) {
  
        case 'nr_rejestracyjny' :
          echo '<a href="'.admin_url('post.php?post='.get_the_ID().'&action=edit').'">'.get_post_meta( $post_id , 'samochod_numer_rejestracyjny', true).'</a>';
        break;

        case 'cena_od' :
    
          if(!empty(get_post_meta( $post_id , 'coast_hours_0_coast_rent', true))): echo '<a href="'.admin_url('post.php?post='.get_the_ID().'&action=edit').'">'.number_format_i18n( preg_replace("/[^0-9,.]/", "", get_post_meta( $post_id , 'coast_hours_0_coast_rent', true)), 2 ).' zł</a>'; endif;
        break;

        case 'rodzaj' :
          $terms = get_the_term_list( $post_id , 'rodzaj' , '' , ',' , '' );
          if ( is_string( $terms ) )
              echo strip_tags($terms);
          else
              _e( 'Brak', 'your_text_domain' );
          break;
        break;
      }
  }

  // Extend search for 'samochod' post type
  function extend_car_search( $query ) {
    $post_type = 'samochod';
    $custom_fields = array(
          "samochod_numer_rejestracyjny",
      );
  
      if( ! is_admin() )
        return;
      
      if ( $query->query['post_type'] != $post_type )
        return;
  
      $search_term = $query->query_vars['s'];
  
      // Set to empty, otherwise it won't find anything
      $query->query_vars['s'] = '';
  
      if ( $search_term != '' ) {
          $meta_query = array( 'relation' => 'OR' );
  
          foreach( $custom_fields as $custom_field ) {
              array_push( $meta_query, array(
                  'key' => $custom_field,
                  'value' => $search_term,
                  'compare' => 'LIKE'
              ));
          }
  
          $query->set( 'meta_query', $meta_query );
      };
  }
  add_action( 'pre_get_posts', 'extend_car_search' );



// Enable 'samochod' columns sorting
add_filter( 'manage_edit-samochod_sortable_columns', 'my_sortable_samochod_column' );
function my_sortable_samochod_column( $columns ) {
    
  $columns['nr_rejestracyjny'] = 'nr_rejestracyjny';
  $columns['cena_od'] = 'cena_od';
  $columns['rodzaj'] = 'rodzaj';
  
    return $columns;
}

// Sorting query
add_action( 'pre_get_posts', 'car_orderby' );
function car_orderby( $query ) {
    if( ! is_admin() )
        return;
 
    $orderby = $query->get( 'orderby');
 
    if( 'nr_rejestracyjny' == $orderby ) {
      $query->set('meta_key','samochod_numer_rejestracyjny');
      $query->set('orderby','meta_value');
    }

    if( 'cena_od' == $orderby ) {
      $query->set('meta_key','coast_hours_0_coast_rent');
      $query->set('orderby','meta_value_num');
    }

    if( 'rodzaj' == $orderby ) {
      // silence is golden
      // but maybe not this time :P
    }  
}








// 
//  Klienci
//

// Add the custom columns to the samochod post type:
add_filter( 'manage_klient_posts_columns', 'set_custom_edit_klient_columns' );
function set_custom_edit_klient_columns($columns) {
    $columns['osoba_kontakowa'] = __( 'Osoba kontaktowa', 'your_text_domain' );
    $columns['dane_kontaktowe'] = __( 'Dane kontaktowe', 'your_text_domain' );
    $columns['dane_do_umowy'] = __( 'Dane do umowy', 'your_text_domain' );
    $columns['date'];
    return $columns;
}

// Add the data to the custom columns for the samochod post type:
  add_action( 'manage_klient_posts_custom_column' , 'custom_klient_column', 10, 2 );
  function custom_klient_column( $column, $post_id ) {
      switch ( $column ) {

        case 'dane_do_umowy' :
          echo '<a href="'.admin_url('post.php?post='.get_the_id().'&action=edit').'">';
            if(!empty(get_post_meta( $post_id , 'dane-umowa_nip', true))): echo get_post_meta( $post_id , 'dane-umowa_nip', true); endif;
            if(!empty(get_post_meta( $post_id , 'dane-umowa_ulica', true))): echo '<br>'.get_post_meta( $post_id , 'dane-umowa_ulica', true); endif;
            if(!empty(get_post_meta( $post_id , 'dane-umowa_miejscowosc', true))): echo '<br>'.get_post_meta( $post_id , 'dane-umowa_miejscowosc', true); endif; if(!empty(get_post_meta( $post_id , 'dane-umowa_kod_pocztowy', true))): echo ' '.get_post_meta( $post_id , 'dane-umowa_kod_pocztowy', true); endif;
          echo '</a>';
        break;
  
        case 'dane_kontaktowe' :
          echo '<a href="'.admin_url('post.php?post='.get_the_id().'&action=edit').'">';
            if(!empty(get_post_meta( $post_id , 'dane-teleadresowe_telefon_1', true))): echo get_post_meta( $post_id , 'dane-teleadresowe_telefon_1', true); endif;
            if(!empty(get_post_meta( $post_id , 'dane-teleadresowe_telefon_2', true))): echo '<br>'.get_post_meta( $post_id , 'dane-teleadresowe_telefon_2', true); endif;
            if(!empty(get_post_meta( $post_id , 'dane-teleadresowe_e-mail', true))): echo '<br>'.get_post_meta( $post_id , 'dane-teleadresowe_e-mail', true); endif;
          echo '</a>';
        break;

        case 'osoba_kontakowa' :
            echo wpautop('<a href="'.admin_url('post.php?post='.get_the_id().'&action=edit').'">'.get_post_meta( $post_id , 'dane-teleadresowe_osoba_kontaktowa', true).'</a>');
        break;
      }
  }

// Extend search for 'klient' post type
//  function extend_admin_search( $query ) {
//    $post_type = 'klient';
//    $custom_fields = array(
//          "dane-umowa_ulica", "dane-umowa_nip", "dane-umowa_miejscowosc", "dane-umowa_kod_pocztowy", "dane-teleadresowe_telefon_1", "dane-teleadresowe_telefon_2", "dane-teleadresowe_e-mail", "dane-teleadresowe_osoba_kontaktowa"
//      );
//
//      if( ! is_admin() )
//        return;
//
//      if ( $query->query['post_type'] != $post_type )
//        return;
//
//      $search_term = $query->query_vars['s'];
//
//      // Set to empty, otherwise it won't find anything
//      $query->query_vars['s'] = '';
//
//      if ( $search_term != '' ) {
//          $meta_query = array( 'relation' => 'OR' );
//
//          foreach( $custom_fields as $custom_field ) {
//              array_push( $meta_query, array(
//                  'key' => $custom_field,
//                  'value' => $search_term,
//                  'compare' => 'LIKE'
//              ));
//          }
//
//          $query->set( 'meta_query', $meta_query );
//      };
//  }
//  add_action( 'pre_get_posts', 'extend_admin_search' );



// 
//  Wynajem
//

// Numer rejestracyjny samochodu w selectboxie
function my_post_object_result( $title, $post, $field, $post_id ) {
  $title .= ' <strong>'. get_post_meta( $post->ID , 'samochod_numer_rejestracyjny', true).'</strong>';
  return $title;
}
add_filter('acf/fields/post_object/result/key=field_5c8106b80de7e', 'my_post_object_result', 10, 4);

// Add the custom columns to the samochod post type:
add_filter( 'manage_wynajem_posts_columns', 'set_wynajem_edit_samochod_columns' );
function set_wynajem_edit_samochod_columns($columns) {
    unset( $columns['date'] );
    unset( $columns['title'] );
    $columns['umowa'] = __( 'Umowa', 'your_text_domain' );
    $columns['klient'] = __( 'Klient', 'your_text_domain' );
    $columns['samochod'] = __( 'Samochód', 'your_text_domain' );
    $columns['odbior'] = __( 'Wydanie', 'your_text_domain' );
    $columns['zwrot'] = __( 'Zdanie', 'your_text_domain' );
    $columns['podsumowanie'] = __( 'r/m/d-godz:min', 'your_text_domain' );
    return $columns;
}


// Add the data to the custom columns for the wynajem post type:
  add_action( 'manage_wynajem_posts_custom_column' , 'custom_wynajem_column', 10, 2 );
  function custom_wynajem_column( $column, $post_id ) {
      switch ( $column ) {
  
        case 'umowa':
          echo '<a href="'.admin_url('post.php?post='.get_the_ID().'&action=edit').'"><strong>'.get_post_meta( $post_id , 'numer_umowy', true).'/'.get_the_date( 'm/Y' ).'</strong></a>';
        break;

        case 'klient':
          //echo get_post_meta( $post_id , 'szczegoly2_klient', true);
          echo wpautop('<a href="'.admin_url('post.php?post='.get_post_meta( $post_id , 'szczegoly2_klient', true).'&action=edit').'">'.
            '<strong>'.get_the_title(get_post_meta( $post_id , 'szczegoly2_klient', true)).'</strong><br>'.
            get_post_meta( get_post_meta( $post_id , 'szczegoly2_klient', true) , 'dane-teleadresowe_osoba_kontaktowa', true).'</a>');
        break;

        case 'samochod':
          echo '<a href="'.admin_url('post.php?post='.get_post_meta( $post_id , 'szczegoly2_samochod', true).'&action=edit').'"><strong>'.get_the_title(get_post_meta( $post_id , 'szczegoly2_samochod', true)).'</strong>'.'<br>'.
          get_post_meta( get_post_meta( $post_id , 'szczegoly2_samochod', true) , 'samochod_numer_rejestracyjny', true).'</a>';
        break;

        case 'odbior':
          echo '<a href="'.admin_url('post.php?post='.get_the_ID().'&action=edit').'">'
          .date('d.m.Y H:i:s',strtotime(get_post_meta( $post_id , 'wydanie_wydanie_data', true))).'<br>'.
          get_post_meta( $post_id , 'wydanie_wydanie_miejsce', true).
          '</a>';
        break;

        case 'zwrot':
          echo '<a href="'.admin_url('post.php?post='.get_the_ID().'&action=edit').'">'
            . date('d.m.Y H:i:s',strtotime(get_post_meta( $post_id , 'zdanie_zdanie_data', true))).'<br>'.
            get_post_meta( $post_id , 'zdanie_zdanie_miejsce', true).
          '</a>';
        break;

        case 'podsumowanie':
          $interval = date_diff(new DateTime(get_post_meta( $post_id , 'zdanie_zdanie_data', true)),new DateTime(get_post_meta( $post_id , 'wydanie_wydanie_data', true)));
          echo $interval->format('%y - %m - %d - %H:%I');
        break;

        break;
      }
  }


  // force one-column dashboard
function shapeSpace_screen_layout_columns($columns) {
	$columns['dashboard'] = 1;
	return $columns;
}
add_filter('screen_layout_columns', 'shapeSpace_screen_layout_columns');

function shapeSpace_screen_layout_dashboard() { return 1; }
add_filter('get_user_option_screen_layout_dashboard', 'shapeSpace_screen_layout_dashboard');


// 
//  Dashboard
//
  
add_action( 'wp_dashboard_setup', 'ci_dashboard_add_widgets' );
function ci_dashboard_add_widgets() {
  wp_add_dashboard_widget( 'dw_dashboard_widget_news', __( 'Aktualne wynajmy', 'dw' ), 'dw_dashboard_widget_news_handler' );
}
  
function dw_dashboard_widget_news_handler() { ?>
<div class="gantt"></div>
<?php }

function my_admin_theme_style() {
  wp_enqueue_style( 'gantt-css', get_template_directory_uri() . '/dist/css/gantt.css', '', '' );
  wp_enqueue_script( 'ganatt-js', get_template_directory_uri() . '/dist/js/jquery.fn.gantt.min.js', array( 'jquery' ), '', true );
  wp_enqueue_script( 'test', get_template_directory_uri() . '/dist/js/test.js', '', '', true );
}
add_action('admin_enqueue_scripts', 'my_admin_theme_style');
add_action('login_enqueue_scripts', 'my_admin_theme_style');


// Dodawanie numeru umowy 
function numer_umowy($post_id){
    $post_type = get_post_type($post_id);

    if($post_type == 'wynajem') {
        $count_posts = wp_count_posts('wynajem')->publish;
        $total_posts = $count_posts+1;

        add_post_meta($post_id,'numer_umowy', $total_posts, true);
    }
}
add_action( 'save_post', 'numer_umowy' );


// Usunięcie szybkiej edycji dla umów
add_filter( 'post_row_actions', 'remove_row_actions', 10, 2 );

function remove_row_actions( $unset_actions, $post ) {
  global $current_screen;

	if ( $current_screen->post_type != 'wynajem' ) return $unset_actions;
    unset( $unset_actions[ 'inline hide-if-no-js' ] );
	return $unset_actions;
}


// Dodanie przycisku generuj umowę na umowach
function my_duplicate_post_link($actions, $post)
{
    if ($post->post_type=='wynajem')
    {
      $actions['print'] = '<a href="'.get_template_directory_uri().'/print/tcpdf/examples/umowa.php?id='.get_the_id().'" target="_blank" title="Generuj umowę" rel="permalink">Generuj umowę</a>';
    }
    return $actions;
}
add_filter('post_row_actions', 'my_duplicate_post_link', 10, 2);


// 
add_action( 'admin_menu', 'remove_author_metabox' );
add_action( 'post_submitbox_misc_actions', 'move_author_to_publish_metabox' );
function remove_author_metabox() {
    remove_meta_box( 'authordiv', 'post', 'normal' );
}
function move_author_to_publish_metabox() {
    global $post_ID;
    $post = get_post( $post_ID );
    echo '<div id="author" class="misc-pub-section" style="border-top-style:solid; border-top-width:1px; border-top-color:#EEEEEE; border-bottom-width:0px; padding-top: 15px; padding-bottom: 20px;">Wynajmujący: ';
    post_author_meta_box( $post );
    echo '</div>';
    if ($post->post_type=='wynajem')
    {
      echo '<div id="umowa" class="misc-pub-section" style="border-top-style:solid; border-top-width:1px; border-top-color:#EEEEEE; border-bottom-width:0px; padding-top: 15px; padding-bottom: 20px;"><a href="'.get_template_directory_uri().'/print/tcpdf/examples/umowa.php?id='.get_the_id().'" target="_blank" class="button button-primary button-large">Generuj umowę</a></div>';
    }
}


// Dodawanie numeru telefono do uytkownika
add_action( 'show_user_profile', 'my_show_extra_profile_fields' );
add_action( 'edit_user_profile', 'my_show_extra_profile_fields' );
function my_show_extra_profile_fields( $user ) { ?>
<h3>Kontakt</h3>
    <table class="form-table">
<tr>
            <th><label for="phone">Numer telefonu</label></th>
            <td>
            <input type="text" name="phone" id="phone" value="<?php echo esc_attr( get_the_author_meta( 'phone', $user->ID ) ); ?>" class="regular-text" /><br />
                <span class="description">Proszę podać numer telefonu</span>
            </td>
</tr>
</table>
<?php }


// Zapisywanie numeru telefonu do bazy
add_action( 'personal_options_update', 'my_save_extra_profile_fields' );
add_action( 'edit_user_profile_update', 'my_save_extra_profile_fields' );

function my_save_extra_profile_fields( $user_id ) {

if ( !current_user_can( 'edit_user', $user_id ) )
    return false;

update_usermeta( $user_id, 'phone', $_POST['phone'] );
}


function cptui_register_my_cpts() {

	/**
	 * Post Type: Wynajem.
	 */

	$labels = array(
		"name" => __( "Wynajem", "custom-post-type-ui" ),
		"singular_name" => __( "Wynajem", "custom-post-type-ui" ),
		"menu_name" => __( "Wynajem", "custom-post-type-ui" ),
		"all_items" => __( "Wszystkie", "custom-post-type-ui" ),
		"add_new" => __( "Nowy", "custom-post-type-ui" ),
		"add_new_item" => __( "Nowy", "custom-post-type-ui" ),
		"edit_item" => __( "Edytuj", "custom-post-type-ui" ),
		"new_item" => __( "Nowy", "custom-post-type-ui" ),
		"view_item" => __( "Zobacz", "custom-post-type-ui" ),
		"view_items" => __( "Zobacz", "custom-post-type-ui" ),
		"search_items" => __( "Szukaj", "custom-post-type-ui" ),
		"not_found" => __( "Nie znaleziono", "custom-post-type-ui" ),
		"not_found_in_trash" => __( "Nie znaleziono w koszu", "custom-post-type-ui" ),
		"parent_item_colon" => __( "Rodzic", "custom-post-type-ui" ),
		"parent_item_colon" => __( "Rodzic", "custom-post-type-ui" ),
	);

	$args = array(
		"label" => __( "Wynajem", "custom-post-type-ui" ),
		"labels" => $labels,
		"description" => "",
		"public" => false,
		"publicly_queryable" => false,
		"show_ui" => true,
		"delete_with_user" => false,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => false,
		"exclude_from_search" => true,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => false,
		"query_var" => true,
		"menu_position" => 5,
		"menu_icon" => "dashicons-clipboard",
		"supports" => array( "title", "editor", "thumbnail", "custom-fields" ),
	);

	register_post_type( "wynajem", $args );

	/**
	 * Post Type: Klienci.
	 */

	$labels = array(
		"name" => __( "Klienci", "custom-post-type-ui" ),
		"singular_name" => __( "Klient", "custom-post-type-ui" ),
		"menu_name" => __( "Klienci", "custom-post-type-ui" ),
		"all_items" => __( "Wszyscy", "custom-post-type-ui" ),
		"add_new" => __( "Nowy klient", "custom-post-type-ui" ),
		"add_new_item" => __( "Nowy klient", "custom-post-type-ui" ),
		"edit_item" => __( "Edytuj klienta", "custom-post-type-ui" ),
		"new_item" => __( "Nowy klient", "custom-post-type-ui" ),
		"view_item" => __( "Zobacz", "custom-post-type-ui" ),
		"view_items" => __( "Zobacz", "custom-post-type-ui" ),
		"search_items" => __( "Szukaj", "custom-post-type-ui" ),
		"not_found" => __( "Nie znaleziono", "custom-post-type-ui" ),
		"not_found_in_trash" => __( "Nie znaleziono w koszu", "custom-post-type-ui" ),
		"parent_item_colon" => __( "Rodzic", "custom-post-type-ui" ),
		"featured_image" => __( "Obrazek wyróżniający", "custom-post-type-ui" ),
		"set_featured_image" => __( "Ustaw obrazek wyrużniający", "custom-post-type-ui" ),
		"parent_item_colon" => __( "Rodzic", "custom-post-type-ui" ),
	);

	$args = array(
		"label" => __( "Klienci", "custom-post-type-ui" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => false,
		"show_ui" => true,
		"delete_with_user" => false,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => false,
		"exclude_from_search" => true,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => false,
		"query_var" => true,
		"menu_position" => 7,
		"menu_icon" => "dashicons-businessman",
		"supports" => array( "title", "editor", "thumbnail", "custom-fields" ),
	);

	register_post_type( "klient", $args );

	/**
	 * Post Type: Samochody.
	 */

	$labels = array(
		"name" => __( "Samochody", "custom-post-type-ui" ),
		"singular_name" => __( "Samochód", "custom-post-type-ui" ),
		"menu_name" => __( "Samochody", "custom-post-type-ui" ),
		"all_items" => __( "Wszystkie", "custom-post-type-ui" ),
		"add_new" => __( "Dodaj nowy", "custom-post-type-ui" ),
		"add_new_item" => __( "Dodaj nowy", "custom-post-type-ui" ),
		"edit_item" => __( "Edytuj", "custom-post-type-ui" ),
		"new_item" => __( "Dodaj nowy", "custom-post-type-ui" ),
		"view_item" => __( "Zobacz", "custom-post-type-ui" ),
		"view_items" => __( "Zobacz", "custom-post-type-ui" ),
		"search_items" => __( "Szukaj", "custom-post-type-ui" ),
		"not_found" => __( "Nie znaleziono", "custom-post-type-ui" ),
		"not_found_in_trash" => __( "Nie znaleziono w koszu", "custom-post-type-ui" ),
		"parent_item_colon" => __( "Rodzic", "custom-post-type-ui" ),
		"featured_image" => __( "Obrazek wyróżniający", "custom-post-type-ui" ),
		"set_featured_image" => __( "Ustaw obrazek wyrużniający", "custom-post-type-ui" ),
		"remove_featured_image" => __( "Usuń obrazek wyróżniający", "custom-post-type-ui" ),
		"use_featured_image" => __( "Użyj obrazek wyróżniający", "custom-post-type-ui" ),
		"archives" => __( "Archiva", "custom-post-type-ui" ),
		"parent_item_colon" => __( "Rodzic", "custom-post-type-ui" ),
	);

	$args = array(
		"label" => __( "Samochody", "custom-post-type-ui" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"delete_with_user" => false,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => "samochody",
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => true,
		"rewrite" => array( "slug" => "samochod", "with_front" => true ),
		"query_var" => true,
		"menu_position" => 6,
		"menu_icon" => "dashicons-dashboard",
		"supports" => array( "title", "editor", "thumbnail", "custom-fields" ),
		"taxonomies" => array( "rodzaj" ),
	);

	register_post_type( "samochod", $args );
}

add_action( 'init', 'cptui_register_my_cpts' ); 

add_action( 'edit_form_after_title', 'add_content_before_editor' );
function add_content_before_editor() {
  global $post;
  if (get_post_type() == "wynajem"): 
    echo '<div class="koszty-wynajmu">';

      $interval = date_diff(new DateTime(get_post_meta( get_the_ID() , 'zdanie_zdanie_data', true)),new DateTime(get_post_meta( get_the_ID() , 'wydanie_wydanie_data', true)));

      $array = get_field('coast_hours', get_post_meta( get_the_ID() , 'szczegoly2_samochod', true));
      $price = 0;
      $rental_cost = 0;

//        if(!$array){
//            echo '<h1>0 zł netto / 0 brutto</h1>';
//            echo '<br><strong>Najpierw wybierz samochód, potem ustawiaj datę!</strong><br>';
//            return;
//        }
      // Pętla
      foreach (@$array as $key):

        // Lata
        if ($interval->format('%y') > 0):
          if (in_array("rok", $key)) {
            $oddo = explode("-", $key["time_rent"]);
              if ($interval->format('%y') >= $oddo[0]) {
                $rental_cost = intval($key["coast_rent"]);
                $price = $price + intval($interval->format('%y')) * $rental_cost;
              }
          }
        endif;

        // Miesiące
        if ($interval->format('%m') > 0):
          if (in_array("miesiac", $key)) {
            $oddo = explode("-", $key["time_rent"]);
              if (($interval->format('%m') >= $oddo[0]) && ($interval->format('%m') <= $oddo[1])) {
                $rental_cost = intval($key["coast_rent"]);
                $price = $price + intval($interval->format('%m')) * $rental_cost;
              }
          }
        endif;

        // Dni
        if ($interval->format('%d') > 0):
          if (in_array("dzien", $key)) {
            $oddo = explode("-", $key["time_rent"]);
              if (($interval->format('%d') >= $oddo[0]) && ($interval->format('%d') <= $oddo[1])) {
                $rental_cost = intval($key["coast_rent"]);
                $price = $price + intval($interval->format('%d')) * $rental_cost;
              }
          }
        endif;
      endforeach;

      // Godziny
      if ($interval->format('%H') > 0):
        $price = $price + $interval->format('%H') * ($rental_cost / 24);
      endif;

      // Minuty
      if ($interval->format('%I') > 0): 
        $price = $price + $interval->format('%I') * ($rental_cost / (24 * 60));
      endif;

      if (get_post_meta( get_the_ID() , 'szczegoly2_cena_indywidualna', true)):
        echo '<h1>'.number_format((float)round(get_post_meta( get_the_ID() , 'szczegoly2_cena_indywidualna', true), 2), 2, '.', '').' zł netto / '.number_format((float)(get_post_meta( get_the_ID() , 'szczegoly2_cena_indywidualna', true) + (round(get_post_meta( get_the_ID() , 'szczegoly2_cena_indywidualna', true), 2) / 100) * 23), 2, '.', '').' brutto</h1>';
      else: 
        echo '<h1>'.number_format((float)round($price, 2), 2, '.', '').' zł netto / '.number_format((float)($price + (round($price, 2) / 100) * 23), 2, '.', '').' brutto</h1>';
      endif;

      echo 'Okres wynajmu: lata - '.$interval->format('%y').', miesiące - '.$interval->format('%m').', dni - '.$interval->format('%d').', godz:min - '.$interval->format('%H:%I').'<br>';
      echo '<br><strong>Najpierw wybierz samochód, potem ustawiaj datę!</strong><br>';

      // if( have_rows('coast_hours', get_post_meta( get_the_ID() , 'szczegoly2_samochod', true) ) ):
      // echo '<ul>';

      //   while( have_rows('coast_hours', get_post_meta( get_the_ID() , 'szczegoly2_samochod', true)) ): the_row();

      //   echo '<li>'.get_sub_field('time_rent').' '.get_sub_field('okres').' '.get_sub_field('coast_rent').'</li>';

      // endwhile;
      // echo '</ul>';
      // endif;





    echo '</div>';
  endif;
}


add_action( 'rest_api_init', function () {
    register_rest_route( 'v1', '/changePrice', array(
        'methods' => 'POST',
        'callback' => 'changePricee',
    ) );
} );

add_action( 'rest_api_init', function () {
    register_rest_route( 'v1', '/getpost/(?P<slug>[^.]+)', array(
        'methods' => 'GET',
        'callback' => 'getPost',
    ) );
} );

function getPost($data){
    return get_field('coast_hours',$data['slug']);
}

function changePricee(){
    $interval = date_diff(new DateTime($_POST['data_wydania'])
        ,new DateTime($_POST['data_zdania']));
    $array = $_POST['coast_hours'];
    $price = 0;
    $rental_cost = 0;

    // Pętla
    foreach ($array as $key):

        // Lata
        if ($interval->format('%y') > 0):
            if (in_array("rok", $key)) {
                $oddo = explode("-", $key["time_rent"]);
                if ($interval->format('%y') >= $oddo[0]) {
                    $rental_cost = intval($key["coast_rent"]);
                    $price = $price + intval($interval->format('%y')) * $rental_cost;
                }
            }
        endif;

        // Miesiące
        if ($interval->format('%m') > 0):
            if (in_array("miesiac", $key)) {
                $oddo = explode("-", $key["time_rent"]);
                if (($interval->format('%m') >= $oddo[0]) && ($interval->format('%m') <= $oddo[1])) {
                    $rental_cost = intval($key["coast_rent"]);
                    $price = $price + intval($interval->format('%m')) * $rental_cost;
                }
            }
        endif;

        // Dni
        if ($interval->format('%d') > 0):
            if (in_array("dzien", $key)) {
                $oddo = explode("-", $key["time_rent"]);
                if (($interval->format('%d') >= $oddo[0]) && ($interval->format('%d') <= $oddo[1])) {
                    $rental_cost = intval($key["coast_rent"]);
                    $price = $price + intval($interval->format('%d')) * $rental_cost;
                }
            }
        endif;
    endforeach;

    // Godziny
    if ($interval->format('%H') > 0):
        $price = $price + $interval->format('%H') * ($rental_cost / 24);
    endif;

    // Minuty
    if ($interval->format('%I') > 0):
        $price = $price + $interval->format('%I') * ($rental_cost / (24 * 60));
    endif;
//    return $interval;
    return '<h1>'.number_format((float)round($price, 2), 2, '.', '').' zł netto / '.number_format((float)($price + (round($price, 2) / 100) * 23), 2, '.', '').' brutto</h1>
    Okres wynajmu: lata - '.$interval->format('%y').', miesiące - '.$interval->format('%m').', dni - '.$interval->format('%d').', godz:min - '.$interval->format('%H:%I').'<br>
    <br><strong>Najpierw wybierz samochód, potem ustawiaj datę!</strong><br>';
}
