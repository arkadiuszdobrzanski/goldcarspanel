<?php


function GetVar() { 

	global $wpdb;

	$parse_uri = explode( 'wp-content', $_SERVER['SCRIPT_FILENAME'] );
	require_once( $parse_uri[0] . 'wp-load.php' );

	$query = "SELECT * FROM {$wpdb->prefix}contracts WHERE ID = ".$_GET['id']; 
	$result = $wpdb->get_results($query); 
	if (count($result)> 0) {
		if (is_user_logged_in() && current_user_can('administrator') || is_user_logged_in() && $umowa->partner == get_current_user_id())
			foreach ( $result as $umowa )
			{
				$content = '<h1>Kalkulacja Nr. '.$umowa->ID.'</h1><br>';
				$content = ' <table style="width:100%">
				<tr>

				<td><strong>'.$umowa->company.'</strong><br>
					'.$umowa->nip.'<br>
					'.$umowa->city.' '.$umowa->postal_code.', '.$umowa->street.'<br>
					'.$umowa->email.' '.$umowa->phone.'</td>
				<td>';
				$users = get_users();
				foreach ( $users as $user ) {
					if ($user->ID == $umowa->partner):
						$content .= '<strong>'.$user->first_name.' '.$user->last_name.'</strong><br>'.date('d.m.Y', strtotime($umowa->date_added));
					endif;
				}
				$content .= '</td>

				</tr>
				</table>
				<br><br>';
				$content .= '
				<table style="width:100%; border: solid 1px black; font-size: 10px;" border="1" cellpadding="5">
					<tr>
						<th style="width: 30%">Stopień niepełnospr.</th>
						<th style="width: 14%">Lekki</th>
						<th style="width: 14%">Umiarkowany</th>
						<th style="width: 14%">Umiarkowany
						szczególnie</th>
						<th style="width: 14%">Znaczny</th>
						<th style="width: 14%">Znaczny
						szczególnie</th>
					</tr>

					<tr>
						<td>Ilość etatów</td>
						<td>'.$umowa->n_lekka.'</td>
						<td>'.$umowa->n_umiarkowana.'</td>
						<td>'.$umowa->n_umiarkowana_szczegolna.'</td>
						<td>'.$umowa->n_znaczna.'</td>
						<td>'.$umowa->n_znaczna_szczegolnie.'</td>
					</tr>

					<tr>
						<td>Miesięczne dof. PFRON na 1 ON</td>
						<td>'.$umowa->dof_n_lekka.'zł</td>
						<td>'.$umowa->dof_n_umiarkowana.'zł</td>
						<td>'.$umowa->dof_n_umiarkowana_szczegolna.'zł</td>
						<td>'.$umowa->dof_n_znaczna.'zł</td>
						<td>'.$umowa->dof_n_znaczna_szczegolnie.'zł</td>
					</tr>

					<tr>
						<td>Miesięczny koszt klienta na 1 ON</td>
						<td colspan="5">'.$umowa->minimalna_krajowa.'zł</td>
					</tr>

					<tr>
						<td>Ilość zat na pełny etat u klienta</td>
						<td colspan="5">'.$umowa->l_etat.'</td>
					</tr>

					<tr>
						<td>Wym liczba zat ON, aby nie płacić skł PFRON (6% zat)</td>
						<td>'.$umowa->wym_zat_1.'</td>
						<td>'.$umowa->wym_zat_2.'</td>
						<td>'.$umowa->wym_zat_3.'</td>
						<td>'.$umowa->wym_zat_4.'</td>
						<td>'.$umowa->wym_zat_5.'</td>
					</tr>

					<tr>
						<td>Pokrycie faktycznie</td>
						<td>'.$umowa->pok_fak_1.'</td>
						<td>'.$umowa->pok_fak_2.'</td>
						<td>'.$umowa->pok_fak_3.'</td>
						<td>'.$umowa->pok_fak_4.'</td>
						<td>'.$umowa->pok_fak_5.'</td>
					</tr>

					<tr>
						<td>Ile powinni mieć</td>
						<td>'.$umowa->ile_pow_miec_1.'</td>
						<td>'.$umowa->ile_pow_miec_2.'</td>
						<td>'.$umowa->ile_pow_miec_3.'</td>
						<td>'.$umowa->ile_pow_miec_4.'</td>
						<td>'.$umowa->ile_pow_miec_5.'</td>
					</tr>

					<tr>
						<td>Ile powinno być dokładnie</td>
						<td>'.$umowa->ile_pow_miec_dok_1.'</td>
						<td>'.$umowa->ile_pow_miec_dok_2.'</td>
						<td>'.$umowa->ile_pow_miec_dok_3.'</td>
						<td>'.$umowa->ile_pow_miec_dok_4.'</td>
						<td>'.$umowa->ile_pow_miec_dok_5.'</td>
					</tr>

					<tr>
						<td>Ile mają</td>
						<td>'.$umowa->ilu_maja_lekka.'</td>
						<td>'.$umowa->ilu_maja_umiarkowany.'</td>
						<td>'.$umowa->ilu_maja_umiarkowany_sz.'</td>
						<td>'.$umowa->ilu_maja_znaczny.'</td>
						<td>'.$umowa->ilu_maja_znaczny_sz.'</td>
					</tr>

					<tr>
						<td>Tylu brakuje</td>
						<td>'.$umowa->tyl_brak_1.'</td>
						<td>'.$umowa->tyl_brak_2.'</td>
						<td>'.$umowa->tyl_brak_3.'</td>
						<td>'.$umowa->tyl_brak_4.'</td>
						<td>'.$umowa->tyl_brak_5.'</td>
					</tr>

					<tr>
						<td>Wymagana liczba zat. ON,
						dla Każdego st niepełnospr.</td>
						<td>-</td>
						<td>-</td>
						<td>'.$umowa->wym_l_on_dla_s_n_3.'</td>
						<td>-</td>
						<td>'.$umowa->wym_l_on_dla_s_n_5.'</td>
					</tr>

					<tr>
						<td>Koszt pracodawcy</td>
						<td>'.$umowa->koszt_prac_1.'zł</td>
						<td>'.$umowa->koszt_prac_2.'zł</td>
						<td>'.$umowa->koszt_prac_3.'zł</td>
						<td>'.$umowa->koszt_prac_4.'zł</td>
						<td>'.$umowa->koszt_prac_5.'zł</td>
					</tr>

					<tr>
						<td>Koszt pracodawcy z SOD/msc</td>
						<td>'.$umowa->koszt_prac_z_sod_1.'zł</td>
						<td>'.$umowa->koszt_prac_z_sod_2.'zł</td>
						<td>-</td>
						<td>'.$umowa->koszt_prac_z_sod_4.'zł</td>
						<td>-</td>
					</tr>

					<tr>
						<td>Roczny koszt ON bez dofinansowania PFRON</td>
						<td>'.$umowa->rocz_koszt_on_bz_pfron_1.'zł</td>
						<td>'.$umowa->rocz_koszt_on_bz_pfron_2.'zł</td>
						<td>'.$umowa->rocz_koszt_on_bz_pfron_3.'zł</td>
						<td>'.$umowa->rocz_koszt_on_bz_pfron_4.'zł</td>
						<td>'.$umowa->rocz_koszt_on_bz_pfron_5.'zł</td>
					</tr>

					<tr>
						<td>Roczny koszt ON z dof. PFRON</td>
						<td>'.$umowa->rocz_koszt_on_z_pfron_1.'zł</td>
						<td>'.$umowa->rocz_koszt_on_z_pfron_2.'zł</td>
						<td>-</td>
						<td>'.$umowa->rocz_koszt_on_z_pfron_4.'zł</td>
						<td>-</td>
					</tr>

					<tr>
						<td>Zysk pracodawcy z SOD/miesiąc</td>
						<td>'.$umowa->zysk_sod_msc_1.'zł</td>
						<td>'.$umowa->zysk_sod_msc_2.'zł</td>
						<td>-</td>
						<td>'.$umowa->zysk_sod_msc_4.'zł</td>
						<td>-</td>
					</tr>

					<tr>
						<td>PFRON/miesiąc</td>
						<td>'.$umowa->pfron_msc_1.'zł</td>
						<td>'.$umowa->pfron_msc_2.'zł</td>
						<td>'.$umowa->pfron_msc_3.'zł</td>
						<td>'.$umowa->pfron_msc_4.'zł</td>
						<td>'.$umowa->pfron_msc_5.'zł</td>
					</tr>

					<tr>
						<td>Roczna Składka PFRON Klienta dla etatów</td>
						<td>'.$umowa->pfron_rok_1.'zł</td>
						<td>'.$umowa->pfron_rok_2.'zł</td>
						<td>'.$umowa->pfron_rok_3.'zł</td>
						<td>'.$umowa->pfron_rok_4.'zł</td>
						<td>'.$umowa->pfron_rok_5.'zł</td>
					</tr>

					<tr>
						<td>Zysk PFRON bez SOD/miesiąc</td>
						<td>-</td>
						<td></td>
						<td>'.$umowa->zysk_pfron_bz_sod_3.'zł</td>
						<td>-</td>
						<td>'.$umowa->zysk_pfron_bz_sod_5.'zł</td>
					</tr>

					<tr>
						<td>Oszczędność klienta na PFORN bez dof SOD</td>
						<td>'.$umowa->oszcz_pfron_bez_sod_1.'zł</td>
						<td>'.$umowa->oszcz_pfron_bez_sod_2.'zł</td>
						<td>-</td>
						<td>'.$umowa->oszcz_pfron_bez_sod_4.'zł</td>
						<td>-</td>
					</tr>

					<tr>
						<td>Oszczędność Klienta na PFRON z dof. PFRON</td>
						<td>'.$umowa->oszcz_pfron_z_pfron_1.'zł</td>
						<td>'.$umowa->oszcz_pfron_z_pfron_2.'zł</td>
						<td>-</td>
						<td>'.$umowa->oszcz_pfron_z_pfron_4.'zł</td>
						<td>-</td>
					</tr>

					<tr>
						<td>Oszczędność Klienta na PFRON bez dof PFRON</td>
						<td>-</td>
						<td>-</td>
						<td>'.$umowa->oszcz_pfron_bz_pfron_3.'zł</td>
						<td>-</td>
						<td>'.$umowa->oszcz_pfron_bz_pfron_5.'zł</td>
					</tr>

					<tr>
						<td>Wartość Rocznej Prowizji evoSOLUTIONS (15% składki rocznej klienta)</td>
						<td>'.$umowa->prow_evo_1.'zł</td>
						<td>'.$umowa->prow_evo_2.'zł</td>
						<td>'.$umowa->prow_evo_3.'zł</td>
						<td>'.$umowa->prow_evo_4.'zł</td>
						<td>'.$umowa->prow_evo_5.'zł</td>
					</tr>

					<tr>
						<td>Prowizja Partnera</td>
						<td>'.$umowa->oszcz_skl_pfron_1.'zł</td>
						<td>'.$umowa->oszcz_skl_pfron_2.'zł</td>
						<td>'.$umowa->oszcz_skl_pfron_3.'zł</td>
						<td>'.$umowa->oszcz_skl_pfron_4.'zł</td>
						<td>'.$umowa->oszcz_skl_pfron_5.'zł</td>
					</tr>

					<tr>
						<td>Wartość Rocznej Oszczędności Klienta jako % składki PFRON</td>
						<td>'.$umowa->oszcz_proc_skl_pfron_1.'zł</td>
						<td>'.$umowa->oszcz_proc_skl_pfron_2.'zł</td>
						<td>'.$umowa->oszcz_proc_skl_pfron_3.'zł</td>
						<td>'.$umowa->oszcz_proc_skl_pfron_4.'zł</td>
						<td>'.$umowa->oszcz_proc_skl_pfron_5.'zł</td>
					</tr>

				</table> ';
			}
		}

		return $content;

}


// Include the main TCPDF library (search for installation path).
require_once('tcpdf_include.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);




// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetTitle('Umowa');
$pdf->SetSubject('');
$pdf->SetKeywords('');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, 50);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/pol.php')) {
    require_once(dirname(__FILE__).'/lang/pol.php');
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf->SetFont('dejavusans', '', 9);

// add a page
$pdf->AddPage();

// writeHTML($html, $ln=true, $fill=false, $reseth=false, $cell=false, $align='')
// writeHTMLCell($w, $h, $x, $y, $html='', $border=0, $ln=0, $fill=0, $reseth=true, $align='', $autopadding=true)

// create some HTML content
$html = GetVar();

// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');

// reset pointer to the last page
$pdf->lastPage();

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// Print a table




// ---------------------------------------------------------

//Close and output PDF document
$pdf->Output('umowa.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+