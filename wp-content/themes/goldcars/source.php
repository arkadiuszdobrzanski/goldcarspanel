<?php 
echo '[';

global $wpdb;

$parse_uri = explode( 'wp-content', $_SERVER['SCRIPT_FILENAME'] );
require_once( $parse_uri[0] . 'wp-load.php' );

$loop = new WP_Query( array(
    'post_type' => 'samochod',
    'posts_per_page' => -1,
    'orderby' => 'name',
    'order' => 'ASC'
  )
);
$i = 0;
while ( $loop->have_posts() ) : $loop->the_post();
$i++;

if ($i == 1) {
    echo '{';
} else {
    echo ',{';
}
// name
echo '"name": "'.get_the_title().'",';
// desc
echo '"desc": "'.get_field('samochod_numer_rejestracyjny').'"';


    $loop_wynajem = new WP_Query( array(
        'post_type' => 'wynajem',
        'posts_per_page' => 30,
        'meta_key' => 'szczegoly2_samochod',
        'meta_value' => get_the_ID()
    ));


    if ( $loop_wynajem->have_posts() ) : 
        echo ',"values": [';
        $n = 0;
        while ( $loop_wynajem->have_posts() ) : $loop_wynajem->the_post();
        $n++;

        if ($n == 1) {
            echo '{';
        } else {
            echo ',{';
        }
            echo '"from": "'.get_field('wydanie_wydanie_data').'"';
            echo ',"to": "'.get_field('zdanie_zdanie_data').'"';
            echo ',"label": "'.get_the_id().'"';

        echo '}';

        endwhile;
        echo ']';
    endif;

echo '}';

endwhile; wp_reset_query();

echo ']';
?>